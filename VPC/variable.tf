variable "cidr"  {
    type = "map"
    default = {
        vpc_cidr = "10.0.0.0/16"
        subnet_pri = "10.0.16.0/20"
        subnet_pub = "10.0.32.0/20"
    }
  
}

variable "tag_name" {
    default = "Env_prod"
}
