resource "aws_vpc" "vpc_main" {
    cidr_block = "${var.cidr["vpc_cidr"]}"
    tags {
        Name = "VPC"
        Env = "${var.tag_name}"
    }
  
}

resource "aws_subnet" "sub1_main" {
    vpc_id = "${aws_vpc.vpc_main.id}"
    cidr_block = "${var.cidr["subnet_pri"]}"
    tags {
        Name = "subnet-pub"
        Env = "${var.tag_name}"
    }
    map_public_ip_on_launch = true
  
}
resource "aws_subnet" "sub2_main" {
    vpc_id = "${aws_vpc.vpc_main.id}"
    cidr_block = "${var.cidr["subnet_pub"]}"
    tags{
        Name = "subnet-pri"
        Env = "${var.tag_name}"
    }
  
}
