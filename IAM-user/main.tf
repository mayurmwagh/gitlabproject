provider "aws"{
    region = "eu-west-2"
}

resource "aws_iam_user" "tf-user" {
  name = "user1"
}

data "aws_iam_policy_document" "example" {
  statement {
    actions = [
      "ec2:Describe*"]
    resources = [
      "*"]
  }
}

resource "aws_iam_policy" "tf-policy" {
 name = "ec2-read-only"
 policy = "${data.aws_iam_policy_document.example.json}"
}