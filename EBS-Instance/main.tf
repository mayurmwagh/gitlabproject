provider "aws" {
  #version = "2.62.0"
  region = "eu-west-2"
}

resource "aws_instance" "web-2" {
    ami = "ami-09e5afc68eed60ef4"
    instance_type = "t2.micro"
    availability_zone = "eu-west-2a"

    
    tags {
        Name = "server2"
        Env = "prod"
    }
  
}

resource "aws_ebs_volume" "data_vol"{
    availability_zone = "eu-west-2a"
    size =1
    tags= {
        Name = "volume"
    }
}

resource "aws_volume_attchment" "web-2"{
    device_name="dev/sdc"
    volume_id ="${aws_ebs_volume.data_vol.id}"
    instance_id="${aws_instance.web-2.id}"
}




